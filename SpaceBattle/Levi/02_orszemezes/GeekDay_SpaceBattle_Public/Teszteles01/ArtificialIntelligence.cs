﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceBattle.Common;
using System.Drawing;

namespace Teszteles01
{
    static class ArtificialIntelligence
    {
        public static class Adatfeldolgozas
        {
            public static void Feldolgozas(List<GameItemDescriptor> gameItems)
            {
                Feldolgozo feldolgozo = new Feldolgozo(gameItems, Settings.ClientName);

                OwnPlanets = feldolgozo.OwnPlanets;
                OwnShips = feldolgozo.OwnShips;
                EnemyPlanets = feldolgozo.EnemyPlanets;
                EnemyShips = feldolgozo.EnemyShips;
                NeutralPlanets = feldolgozo.NeutralPlanets;

                PlanetMapGeneralas();
                ScoutMapGeneralas();
            }
            static void PlanetMapGeneralas()
            {
                if (!PlanetMap.Initialized)
                    PlanetMap.Initialize(OwnPlanets, EnemyPlanets, NeutralPlanets);
                else
                {
                    PlanetMap.Refresh(OwnPlanets, EnemyPlanets, NeutralPlanets);
                    PlanetMap.FajlbaIr(Settings.FilePaths.PlanetMapTXT);
                }
            }
            static void ScoutMapGeneralas()
            {
                ScoutMap.Initialize(FelderitokDetektalasa(), Settings.MapSizeX, Settings.MapSizeY);
                ScoutMap.FajlbaIr(Settings.FilePaths.ScoutMapTXT);

                ScoutMap.ScoutsNeeded = (Settings.MapSizeX / ShipLatotav) + (Settings.MapSizeX % ShipLatotav == 0 ? 0 : 1);
            }
        }
        static class Felderites
        {
            public static List<BattleCommand> Felderit()
            {
                if (Settings.SPEED == 0)
                {
                    return Traffipax.Indit();
                }
                else
                {
                    return FelderitesPaszta();
                }
                //return FelderitesRandom();
                //return FelderitesFesu();

                //return new List<BattleCommand>();
            }

            static List<OwnShip> Orszemek = new List<OwnShip>();
            static List<int> KikuldottOrszemek = new List<int>();
            static bool OrszemFigyel(float x, float y)
            {
                foreach (OwnShip item in Orszemek)
                {
                    if (item.PosX == x && item.PosY == y || item.DestinationX == x && item.DestinationY == y)
                        return true;
                }
                return false;
            }
            public static List<BattleCommand> Orszemkihelyez()
            {

                Orszemek = OrszemekDetektalasa();
                List<BattleCommand> parancsok = new List<BattleCommand>();
                if (Orszemek.Count < PlanetMap.PermaNeutralPlanets.Count)
                {
                    for (int i = 0; i < PlanetMap.PermaNeutralPlanets.Count - Orszemek.Count; i++)
                    {
                        OwnPlanet legnagyobb = OwnPlanets.Legnagyobb();
                        if (legnagyobb.NumberOfUnits * Settings.MaxOrszemSzazalek / 100 > 3)
                        {
                            parancsok.Add(Commands.Split(legnagyobb.ItemId, 3));
                        }
                    }
                }
                for (int i = 0; i < Orszemek.Count; i++)
                {
                    if (!KikuldottOrszemek.Contains(Orszemek[i].ItemId) && Orszemek[i].DestinationX == -1 && Orszemek[i].DestinationY == -1)
                    {
                        int j = 0;
                        while (j < PlanetMap.PermaNeutralPlanets.Count && OrszemFigyel(PlanetMap.PermaNeutralPlanets[j].PosX, PlanetMap.PermaNeutralPlanets[j].PosY))
                        {
                            j++;
                        }
                        if (j < PlanetMap.PermaNeutralPlanets.Count)
                        {
                            parancsok.Add(Commands.Move(Orszemek[i].ItemId, PlanetMap.PermaNeutralPlanets[j].PosX, PlanetMap.PermaNeutralPlanets[j].PosY));
                            KikuldottOrszemek.Add(Orszemek[i].ItemId);
                        }
                    }
                }
                return parancsok;
            }
            static List<BattleCommand> FelderitesFesu()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //Scoutok létrehozása:
                if (ScoutMap.ActualNumberOfScouts < ScoutMap.ScoutsNeeded)
                {
                    //Annyit választunk le, amennyi még kell ahhoz, hogy elég Felderítőnk legyen
                    int mennyiScoutKellMeg = ScoutMap.ScoutsNeeded - ScoutMap.ActualNumberOfScouts;
                    for (int i = 0; i < mennyiScoutKellMeg; i++)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }
                //Helyre küldés (Ha még nincsenek kiküldve és pont annyi scout van amennyit ki akarunk küldeni)
                if (!ScoutMap.ScoutsOut && ScoutMap.ActualNumberOfScouts == ScoutMap.ScoutsNeeded)
                {
                    for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                    }
                    ScoutMap.ScoutsOut = true;
                }
                //Mozgatás:
                if (ScoutMap.ScoutsOut && ScoutMap.AllScoutsStanding)
                {
                    if (ScoutMap.ScoutsOnRight) //Ha jobb oldalon vannak a scoutok
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                    else
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, Settings.MapSizeX - 1, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                }

                return parancsok;
            }
            static List<BattleCommand> FelderitesRandom()
            {
                const bool FOLYAMATOS_UTANPOTLAS = true;
                const int KEZDETI_FELDERITOK_SZAMA = 5;
                const int MAX_FELDERITOK_SZAMA = 5;
                const int KULDESI_IDOKOZ = 20; //minél nagyobb, annál lassabb, de legalább 2

                List<BattleCommand> parancsok = new List<BattleCommand>();
                List<OwnShip> felderitok = FelderitokDetektalasa();

                if (felderitok.Count() < KEZDETI_FELDERITOK_SZAMA - 1)
                {   //ha még nincs felderítőnk, akkor a bolygónkról válasszunk le egyet
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                }
                else if (Orajel % KULDESI_IDOKOZ == 0)
                {
                    foreach (OwnShip felderito in felderitok)
                    {
                        parancsok.Add(Commands.Move(felderito.ItemId, random.Next(0, 40), random.Next(0, 40)));
                    }
                }

                if (FOLYAMATOS_UTANPOTLAS && felderitok.Count < MAX_FELDERITOK_SZAMA)
                {
                    if (Orajel % (KULDESI_IDOKOZ - 1) == 0)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }

                return parancsok;
            }
            #region Pásztázás
            //static List<int> statuses = new List<int>();
            //static List<int> resetters = new List<int>();
            //static List<int> iterators = new List<int>();
            static int status = 0;
            static int iterator = 1;
            static int resetter = 0;
            static int counter = 0;
            static List<BattleCommand> FelderitesPaszta()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();
                counter++;
                if (ScoutMap.ScoutList.Count < 1)
                {
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                    counter = 0;
                }
                if (ScoutMap.ActualNumberOfScouts != 0)
                {
                    for (int i = 0; i < ScoutMap.ScoutList.Count; i++)
                    {
                        Pasztazo(parancsok, i);
                    }
                }
                return parancsok;
            }
            static void Pasztazo(List<BattleCommand> parancsok, int hanyadik)
            {
                if (iterator < Settings.MapSizeY / 2)
                {
                    if (status < 40 * (1 / Settings.SPEED))
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, -1 + ShipLatotav, (-1 + ShipLatotav * iterator)));

                        status++;
                    }
                    else if (status < 45 * (1 / Settings.SPEED))
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, -1 + ShipLatotav, (ShipLatotav * (iterator + 1) - 1)));
                        status++;
                    }
                    else if (status < 85 * (1 / Settings.SPEED))
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, Settings.MapSizeX - ShipLatotav, (ShipLatotav * (iterator + 1) - 1)));
                        status++;
                    }
                    else if (status < 90 * (1 / Settings.SPEED))
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, Settings.MapSizeX - ShipLatotav, (ShipLatotav * (iterator + 2) - 1)));
                        status++;
                    }
                    else
                    {
                        status = 0;
                        iterator += 2;
                    }
                }
                else
                {
                    if (resetter > 55 * (1 / Settings.SPEED))
                    {
                        iterator = 0;
                        resetter = 0;
                    }
                    else
                    {
                        if (resetter == 0)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, -1 + ShipLatotav, -1 + ShipLatotav));
                        }
                        resetter++;
                    }
                }
            }
            #endregion

        }
        static class Tamadas
        {
            static List<PointF> IdeTamadtunkMar = new List<PointF>();

            public static List<BattleCommand> Tamad()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();
                FarShot.Takaritas(farShots);//FarShot takarítás


                if (Orajel % 50 == 0)
                    Neutralozas();


                #region FarShotok aktiválása
                for (int i = 0; i < farShots.Count; i++)
                    parancsok.Add(farShots[i].Activate(OwnShips));
                #endregion
                return parancsok;
            }
            static void Neutralozas()
            {
                if (PlanetMap.PermaNeutralPlanets.Count >= 1)
                {
                    NeutralPlanet cel = PlanetMap.PermaNeutralPlanets.First();
                    OwnPlanet forras = OwnPlanets.LegkozelebbiAmiNagyobbMint(cel);
                    if (forras != null
                        && !IdeTamadtunkMar.Contains(new PointF(cel.PosX, cel.PosY))   //ha még nem támadtunk ide
                        && (forras.NumberOfUnits > cel.NumberOfUnits + 1))          //és tudunk
                    {
                        FarShot fs = new FarShot(forras, cel, cel.NumberOfUnits + 1);
                        farShots.Add(fs);
                        IdeTamadtunkMar.Add(new PointF(cel.PosX, cel.PosY));
                    }
                }
            }
        }
        static class TranszferTeszt
        {
            public static List<BattleCommand> TranszferProba()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                if (OwnPlanets.Count > 1)
                {

                }

                return parancsok;
            }
        }

        public static int Orajel { get; set; }
        static int ShipLatotav = 2;
        static Random random = new Random();

        static List<FarShot> farShots = new List<FarShot>();

        #region Actual Item Lists

        public static List<OwnPlanet> OwnPlanets = new List<OwnPlanet>();
        public static List<OwnShip> OwnShips = new List<OwnShip>();

        static List<EnemyPlanet> EnemyPlanets = new List<EnemyPlanet>();
        static List<EnemyShip> EnemyShips = new List<EnemyShip>();

        static List<NeutralPlanet> NeutralPlanets = new List<NeutralPlanet>();

        #endregion

        public static List<BattleCommand> KiadottParancsok()
        {
            List<BattleCommand> parancsok = new List<BattleCommand>();

            parancsok = parancsok.Concat(Felderites.Felderit()).ToList();
            parancsok = parancsok.Concat(Felderites.Orszemkihelyez()).ToList();
            //parancsok = parancsok.Concat(Tamadas.Tamad()).ToList();
            //parancsok = parancsok.Concat(TranszferTeszt.TranszferProba()).ToList();

            return parancsok;
        }

        /// <summary>
        /// Megadja a kiküldött felderítőinket. Felderítő = 1 energiaszintű saját hajó
        /// </summary>
        /// <returns></returns>
        static List<OwnShip> FelderitokDetektalasa()
        {
            List<OwnShip> lista = new List<OwnShip>();

            lista = (from x in OwnShips
                     where x.NumberOfUnits == 1      //vagy hogyan máshogy detektáljuk a felderítőinket?
                     select x).ToList();

            return lista;
        }

        static List<OwnShip> OrszemekDetektalasa()
        {
            return (from x in OwnShips
                    where x.NumberOfUnits == 3
                    select x).ToList();
        }
    }
}