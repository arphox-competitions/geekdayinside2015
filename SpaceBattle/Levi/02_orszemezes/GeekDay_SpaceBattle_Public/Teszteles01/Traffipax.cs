﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceBattle.Common;
using System.Drawing;

namespace Teszteles01
{
    static class Traffipax
    {
        static int counter = 0;
        static PointF traffiElozo = new PointF();
        static PointF traffiMost = new PointF();

        public static List<BattleCommand> Indit()
        {
            List<BattleCommand> parancsok = new List<BattleCommand>();

            if (counter <= 4)
            {
                if (counter == 0)
                { parancsok.Add(Commands.Split(ArtificialIntelligence.OwnPlanets.First().ItemId, 1)); counter++; }
                else if (counter == 1)
                {
                    if (ArtificialIntelligence.OwnShips.Count != 0)
                    {
                        parancsok.Add(Commands.Move(ArtificialIntelligence.OwnShips.First().ItemId, ArtificialIntelligence.OwnPlanets.First().PosX + 5, ArtificialIntelligence.OwnPlanets.First().PosY + 5));
                        counter++;
                    }
                }
                else if (counter == 2)
                {
                    OwnShip f = ArtificialIntelligence.OwnShips.First();
                    traffiElozo = new PointF(f.PosX, f.PosY);
                    counter++;
                }
                else if (counter == 3)
                {
                    OwnShip f = ArtificialIntelligence.OwnShips.First();
                    traffiMost = new PointF(f.PosX, f.PosY);
                    counter++;
                }
                else
                {
                    Settings.SPEED = (float)Matek.KetPontTavolsaga(traffiElozo.X, traffiElozo.Y, traffiMost.X, traffiMost.Y);
                    counter++;
                }                
            }

            return parancsok;
        }
        public static bool Vege
        {
            get
            {
                return counter > 4;
            }
        }
    }
}