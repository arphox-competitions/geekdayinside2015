﻿using System.Drawing;

namespace Teszteles01
{
    /// <summary>
    /// Statikus osztály a beállítások tárolására
    /// </summary>
    static class Settings
    {
        public static string ClientName = "RED";
        public static Brush ClientBrush = Brushes.Red;
        public static int MapSizeX = 0;
        public static int MapSizeY = 0;

        public static float SPEED = 0f;
        public static float INTERVAL = 30.8f;  //in ms

        public static int MaxOrszemSzazalek = 25;

        public static class FilePaths
        {
            static string Folder = @"";

            public static string CommandsTXT = Folder + ClientName + "commands.txt";
            public static string PlanetMapTXT = Folder + ClientName + "planetMap.txt";
            public static string ScoutMapTXT = Folder + ClientName + "scoutMap.txt";
            public static string ExceptionTXT = Folder + ClientName + "exceptions.txt";
        }

        public static class FarShots
        {
            public static bool FAJLBA_NAPLOZAS = true;
        }
    }
}