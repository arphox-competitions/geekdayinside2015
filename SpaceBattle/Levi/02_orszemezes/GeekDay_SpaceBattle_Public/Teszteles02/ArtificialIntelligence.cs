﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceBattle.Common;
using System.Drawing;
using System.IO;

namespace Teszteles02
{
    static class ArtificialIntelligence
    {
        static int traffipax = 0;
        static PointF traffiElozo = new PointF(); static PointF traffiMost = new PointF();

        public static class Adatfeldolgozas
        {
            public static void Feldolgozas(List<GameItemDescriptor> gameItems)
            {
                Feldolgozo feldolgozo = new Feldolgozo(gameItems, Settings.ClientName);

                OwnPlanets = feldolgozo.OwnPlanets;
                OwnShips = feldolgozo.OwnShips;
                EnemyPlanets = feldolgozo.EnemyPlanets;
                EnemyShips = feldolgozo.EnemyShips;
                NeutralPlanets = feldolgozo.NeutralPlanets;

                PlanetMapGeneralas();
                ScoutMapGeneralas();
            }
            static void PlanetMapGeneralas()
            {
                if (!PlanetMap.Initialized)
                    PlanetMap.Initialize(OwnPlanets, EnemyPlanets, NeutralPlanets);
                else
                {
                    PlanetMap.Refresh(OwnPlanets, EnemyPlanets, NeutralPlanets);
                    PlanetMap.FajlbaIr(Settings.FilePaths.PlanetMapTXT);
                }
            }
            static void ScoutMapGeneralas()
            {
                ScoutMap.Initialize(FelderitokDetektalasa(), Settings.MapSizeX, Settings.MapSizeY);
                ScoutMap.FajlbaIr(Settings.FilePaths.ScoutMapTXT);

                ScoutMap.ScoutsNeeded = (Settings.MapSizeX / ShipLatotav) + (Settings.MapSizeX % ShipLatotav == 0 ? 0 : 1);
            }
        }
        static class Felderites
        {
            public static List<BattleCommand> Felderit()
            {
                //return FelderitesRandom();
                //return FelderitesFesu();
                return new List<BattleCommand>();
            }

            /// <summary>
            /// A legnagyobb bolygóról küld ki felderítőket
            /// </summary>
            /// <returns></returns>
            static List<BattleCommand> FelderitesFesu()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //Scoutok létrehozása:
                if (ScoutMap.ActualNumberOfScouts < ScoutMap.ScoutsNeeded)
                {
                    //Annyit választunk le, amennyi még kell ahhoz, hogy elég Felderítőnk legyen
                    int mennyiScoutKellMeg = ScoutMap.ScoutsNeeded - ScoutMap.ActualNumberOfScouts;
                    for (int i = 0; i < mennyiScoutKellMeg; i++)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }
                //Helyre küldés (Ha még nincsenek kiküldve és pont annyi scout van amennyit ki akarunk küldeni)
                if (!ScoutMap.ScoutsOut && ScoutMap.ActualNumberOfScouts == ScoutMap.ScoutsNeeded)
                {
                    for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                    }
                    ScoutMap.ScoutsOut = true;
                }
                //Mozgatás:
                if (ScoutMap.ScoutsOut && ScoutMap.AllScoutsStanding)
                {
                    if (ScoutMap.ScoutsOnRight) //Ha jobb oldalon vannak a scoutok
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                    else
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, Settings.MapSizeX - 1, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                }

                return parancsok;
            }
            static List<BattleCommand> FelderitesRandom()
            {
                const bool FOLYAMATOS_UTANPOTLAS = true;
                const int KEZDETI_FELDERITOK_SZAMA = 5;
                const int MAX_FELDERITOK_SZAMA = 5;
                const int KULDESI_IDOKOZ = 20; //minél nagyobb, annál lassabb, de legalább 2

                List<BattleCommand> parancsok = new List<BattleCommand>();
                List<OwnShip> felderitok = FelderitokDetektalasa();

                if (felderitok.Count() < KEZDETI_FELDERITOK_SZAMA - 1)
                {   //ha még nincs felderítőnk, akkor a bolygónkról válasszunk le egyet
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                }
                else if (Orajel % KULDESI_IDOKOZ == 0)
                {
                    foreach (OwnShip felderito in felderitok)
                    {
                        parancsok.Add(Commands.Move(felderito.ItemId, random.Next(0, 40), random.Next(0, 40)));
                    }
                }

                if (FOLYAMATOS_UTANPOTLAS && felderitok.Count < MAX_FELDERITOK_SZAMA)
                {
                    if (Orajel % (KULDESI_IDOKOZ - 1) == 0)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }

                return parancsok;
            }
        }
        static class Tamadas
        {
            static List<PointF> IdeTamadtunkMar = new List<PointF>();
            public static List<BattleCommand> Tamad()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //FarShot takarítás
                FarShot.Takaritas(farShots);

                parancsok = parancsok.Concat(TraffiAgressziv()).ToList();
                //Neutralozas();

                #region FarShot aktiválás
                for (int i = 0; i < farShots.Count; i++)
                {
                    try
                    {
                        parancsok.Add(farShots[i].Activate(OwnShips));
                    }
                    catch (NemEgyertelmuHajo)
                    {
                        using (StreamWriter sw = new StreamWriter(Settings.FilePaths.ExceptionTXT, true))
                        {
                            sw.WriteLine("NemEgyertelmuHajo: " + farShots[i].ToString());
                        }
                    }
                    catch (NemTalalhatoHajo)
                    {
                        using (StreamWriter sw = new StreamWriter(Settings.FilePaths.ExceptionTXT, true))
                        {
                            sw.WriteLine("NemTalalhatoHajo: " + farShots[i].ToString());
                        }
                    }
                }
                #endregion
                return parancsok;
            }
            static List<BattleCommand> TraffiAgressziv()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                if (traffipax == 0)
                { parancsok.Add(Commands.Split(OwnPlanets.First().ItemId, 1)); traffipax++; }
                else if (traffipax == 1)
                {
                    if (OwnShips.Count != 0)
                    {
                        parancsok.Add(Commands.Move(OwnShips.First().ItemId, Settings.MapSizeX - 1, Settings.MapSizeY - 1));
                        traffipax++;
                    }
                }
                else if (traffipax == 2)
                {
                    OwnShip f = OwnShips.First();
                    traffiElozo = new PointF(f.PosX, f.PosY);
                    traffipax++;
                }
                else if (traffipax == 3)
                {
                    OwnShip f = OwnShips.First();
                    traffiMost = new PointF(f.PosX, f.PosY);
                    traffipax++;
                }
                else if (traffipax == 4)
                {
                    Settings.SPEED = (float)Matek.KetPontTavolsaga(traffiElozo.X, traffiElozo.Y, traffiMost.X, traffiMost.Y);
                    traffipax++;
                }

                if (traffipax > 4)
                {
                    if (Orajel % 50 == 0)
                        IdeTamadtunkMar = new List<PointF>();

                    for (int i = 0; i < EnemyPlanets.Count; i++)
                    {
                        OwnPlanet op = OwnPlanets.Legnagyobb();
                        EnemyPlanet ep = EnemyPlanets[i];

                        double distance = Matek.KetItemTavolsaga(ep, op);
                        double speed = Settings.SPEED;
                        double time = distance / speed;
                        //Ez is jónak tűnik
                        //int darab = ep.NumberOfUnits + (int)(time * ep.IncTime) + (int)(1 / ep.IncTime);
                        int darab = ep.NumberOfUnits + (int)(time * ep.IncTime) + (int)(ep.IncTime / 30 * 500);

                        if (op.NumberOfUnits > darab && !IdeTamadtunkMar.Contains(new PointF(ep.PosX, ep.PosY)))
                        {
                            FarShot fs = new FarShot(op, ep, darab);
                            farShots.Add(fs);
                            IdeTamadtunkMar.Add(new PointF(ep.PosX, ep.PosY));
                            break;
                        }
                    }


                }

                return parancsok;
            }
            static void Neutralozas()
            {
                if (PlanetMap.NeutralPlanetsList.Count >= 1)
                {
                    NeutralPlanet cel = PlanetMap.NeutralPlanetsList.First();
                    OwnPlanet forras = OwnPlanets.LegkozelebbiAmiNagyobbMint(cel);
                    if (forras != null
                        && !IdeTamadtunkMar.Contains(new PointF(cel.PosX, cel.PosY))   //ha még nem támadtunk ide
                        && (forras.NumberOfUnits > cel.NumberOfUnits + 1))          //és tudunk
                    {
                        FarShot fs = new FarShot(forras, cel, cel.NumberOfUnits + 1);
                        farShots.Add(fs);
                        IdeTamadtunkMar.Add(new PointF(cel.PosX, cel.PosY));
                    }
                }
            }
        }
















        public static int Orajel { get; set; }
        static int ShipLatotav = 2;
        static Random random = new Random();

        static List<FarShot> farShots = new List<FarShot>();

        #region Actual Item Lists

        static List<OwnPlanet> OwnPlanets = new List<OwnPlanet>();
        static List<OwnShip> OwnShips = new List<OwnShip>();

        static List<EnemyPlanet> EnemyPlanets = new List<EnemyPlanet>();
        static List<EnemyShip> EnemyShips = new List<EnemyShip>();

        static List<NeutralPlanet> NeutralPlanets = new List<NeutralPlanet>();

        #endregion

        public static List<BattleCommand> KiadottParancsok()
        {
            List<BattleCommand> parancsok = new List<BattleCommand>();

            parancsok = parancsok.Concat(Felderites.Felderit()).ToList();
            parancsok = parancsok.Concat(Tamadas.Tamad()).ToList();

            return parancsok;
        }

        /// <summary>
        /// Megadja a kiküldött felderítőinket. Felderítő = 1 energiaszintű saját hajó
        /// </summary>
        /// <returns></returns>
        static List<OwnShip> FelderitokDetektalasa()
        {
            List<OwnShip> lista = new List<OwnShip>();

            lista = (from x in OwnShips
                     where x.NumberOfUnits == 1      //vagy hogyan máshogy detektáljuk a felderítőinket?
                     select x).ToList();

            return lista;
        }
    }
}