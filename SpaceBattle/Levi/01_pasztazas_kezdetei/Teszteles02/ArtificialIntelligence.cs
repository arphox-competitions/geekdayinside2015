﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceBattle.Common;
using System.Drawing;

namespace Teszteles02
{
    static class ArtificialIntelligence
    {
        public static class Adatfeldolgozas
        {
            public static void Feldolgozas(List<GameItemDescriptor> gameItems)
            {
                Feldolgozo feldolgozo = new Feldolgozo(gameItems, Settings.ClientName);

                OwnPlanets = feldolgozo.OwnPlanets;
                OwnShips = feldolgozo.OwnShips;
                EnemyPlanets = feldolgozo.EnemyPlanets;
                EnemyShips = feldolgozo.EnemyShips;
                NeutralPlanets = feldolgozo.NeutralPlanets;

                PlanetMapGeneralas();
                ScoutMapGeneralas();
            }
            static void PlanetMapGeneralas()
            {
                if (!PlanetMap.Initialized)
                    PlanetMap.Initialize(OwnPlanets, EnemyPlanets, NeutralPlanets);
                else
                {
                    PlanetMap.Refresh(OwnPlanets, EnemyPlanets, NeutralPlanets);
                    PlanetMap.FajlbaIr(Settings.FilePaths.PlanetMapTXT);
                }
            }
            static void ScoutMapGeneralas()
            {
                ScoutMap.Initialize(FelderitokDetektalasa(), Settings.MapSizeX, Settings.MapSizeY);
                ScoutMap.FajlbaIr(Settings.FilePaths.ScoutMapTXT);

                ScoutMap.ScoutsNeeded = (Settings.MapSizeX / ShipLatotav) + (Settings.MapSizeX % ShipLatotav == 0 ? 0 : 1);
            }
        }
        static class Felderites
        {
            public static List<BattleCommand> Felderit()
            {
                //return FelderitesRandom();
                //return FelderitesFesu();

                return new List<BattleCommand>();
            }

            /// <summary>
            /// A legnagyobb bolygóról küld ki felderítőket
            /// </summary>
            /// <returns></returns>
            static List<BattleCommand> FelderitesFesu()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //Scoutok létrehozása:
                if (ScoutMap.ActualNumberOfScouts < ScoutMap.ScoutsNeeded)
                {
                    //Annyit választunk le, amennyi még kell ahhoz, hogy elég Felderítőnk legyen
                    int mennyiScoutKellMeg = ScoutMap.ScoutsNeeded - ScoutMap.ActualNumberOfScouts;
                    for (int i = 0; i < mennyiScoutKellMeg; i++)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }
                //Helyre küldés (Ha még nincsenek kiküldve és pont annyi scout van amennyit ki akarunk küldeni)
                if (!ScoutMap.ScoutsOut && ScoutMap.ActualNumberOfScouts == ScoutMap.ScoutsNeeded)
                {
                    for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                    }
                    ScoutMap.ScoutsOut = true;
                }
                //Mozgatás:
                if (ScoutMap.ScoutsOut && ScoutMap.AllScoutsStanding)
                {
                    if (ScoutMap.ScoutsOnRight) //Ha jobb oldalon vannak a scoutok
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                    else
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, Settings.MapSizeX - 1, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                }

                return parancsok;
            }
            static List<BattleCommand> FelderitesRandom()
            {
                const bool FOLYAMATOS_UTANPOTLAS = true;
                const int KEZDETI_FELDERITOK_SZAMA = 5;
                const int MAX_FELDERITOK_SZAMA = 5;
                const int KULDESI_IDOKOZ = 20; //minél nagyobb, annál lassabb, de legalább 2

                List<BattleCommand> parancsok = new List<BattleCommand>();
                List<OwnShip> felderitok = FelderitokDetektalasa();

                if (felderitok.Count() < KEZDETI_FELDERITOK_SZAMA - 1)
                {   //ha még nincs felderítőnk, akkor a bolygónkról válasszunk le egyet
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                }
                else if (Orajel % KULDESI_IDOKOZ == 0)
                {
                    foreach (OwnShip felderito in felderitok)
                    {
                        parancsok.Add(Commands.Move(felderito.ItemId, random.Next(0, 40), random.Next(0, 40)));
                    }
                }

                if (FOLYAMATOS_UTANPOTLAS && felderitok.Count < MAX_FELDERITOK_SZAMA)
                {
                    if (Orajel % (KULDESI_IDOKOZ - 1) == 0)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }

                return parancsok;
            }
        }
        static class Tamadas
        {
            static List<PointF> IdeTamadtunkMar = new List<PointF>();

            public static List<BattleCommand> Tamad()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //FarShot takarítás és aktiválás
                FarShot.Takaritas(farShots);

                #region Működő
                //if (PlanetMap.NeutralPlanetsList.Count >= 1)
                //{
                //    NeutralPlanet cel = PlanetMap.NeutralPlanetsList.First();
                //    OwnPlanet forras = OwnPlanets.LegkozelebbiAmiNagyobbMint(cel);
                //    if (forras != null
                //        && !IdeTamadtunkMar.Contains(new PointF(cel.PosX, cel.PosY))   //ha még nem támadtunk ide
                //        && (forras.NumberOfUnits > cel.NumberOfUnits + 1))          //és tudunk
                //    {
                //        FarShot fs = new FarShot(forras, cel, cel.NumberOfUnits + 1);
                //        farShots.Add(fs);
                //        IdeTamadtunkMar.Add(new PointF(cel.PosX, cel.PosY));
                //    }
                //}
                #endregion

                for (int i = 0; i < EnemyPlanets.Count; i++)
                {
                    OwnPlanet op = OwnPlanets.Legnagyobb();
                    EnemyPlanet ep = EnemyPlanets[i];
                    if (op.NumberOfUnits > ep.NumberOfUnits)
                    {
                        FarShot fs = new FarShot(op, ep, ep.NumberOfUnits);
                        farShots.Add(fs);
                        break;
                    }
                }

                for (int i = 0; i < farShots.Count; i++)
                    parancsok.Add(farShots[i].Activate(OwnShips));

                return parancsok;
            }
        }
















        public static int Orajel { get; set; }
        static int ShipLatotav = 2;
        static Random random = new Random();

        static List<FarShot> farShots = new List<FarShot>();

        #region Actual Item Lists

        static List<OwnPlanet> OwnPlanets = new List<OwnPlanet>();
        static List<OwnShip> OwnShips = new List<OwnShip>();

        static List<EnemyPlanet> EnemyPlanets = new List<EnemyPlanet>();
        static List<EnemyShip> EnemyShips = new List<EnemyShip>();

        static List<NeutralPlanet> NeutralPlanets = new List<NeutralPlanet>();

        #endregion

        public static List<BattleCommand> KiadottParancsok()
        {
            List<BattleCommand> parancsok = new List<BattleCommand>();

            parancsok = parancsok.Concat(Felderites.Felderit()).ToList();
            parancsok = parancsok.Concat(Tamadas.Tamad()).ToList();

            return parancsok;
        }

        /// <summary>
        /// Megadja a kiküldött felderítőinket. Felderítő = 1 energiaszintű saját hajó
        /// </summary>
        /// <returns></returns>
        static List<OwnShip> FelderitokDetektalasa()
        {
            List<OwnShip> lista = new List<OwnShip>();

            lista = (from x in OwnShips
                     where x.NumberOfUnits == 1      //vagy hogyan máshogy detektáljuk a felderítőinket?
                     select x).ToList();

            return lista;
        }
    }
}