﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszteles01
{
    static class Matek
    {
        public static double KetPontTavolsaga(float p1x, float p1y, float p2x, float p2y)
        {
            return Math.Sqrt(Math.Pow((p1x - p2x), 2) + Math.Pow((p1y - p2y), 2));
        }
    }
}
