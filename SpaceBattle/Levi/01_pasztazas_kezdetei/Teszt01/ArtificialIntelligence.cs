﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceBattle.Common;
using System.Drawing;

namespace Teszteles01
{
    static class ArtificialIntelligence
    {
        public static class Adatfeldolgozas
        {
            public static void Feldolgozas(List<GameItemDescriptor> gameItems)
            {
                Feldolgozo feldolgozo = new Feldolgozo(gameItems, Settings.ClientName);

                OwnPlanets = feldolgozo.OwnPlanets;
                OwnShips = feldolgozo.OwnShips;
                EnemyPlanets = feldolgozo.EnemyPlanets;
                EnemyShips = feldolgozo.EnemyShips;
                NeutralPlanets = feldolgozo.NeutralPlanets;

                PlanetMapGeneralas();
                ScoutMapGeneralas();
            }
            static void PlanetMapGeneralas()
            {
                if (!PlanetMap.Initialized)
                    PlanetMap.Initialize(OwnPlanets, EnemyPlanets, NeutralPlanets);
                else
                {
                    PlanetMap.Refresh(OwnPlanets, EnemyPlanets, NeutralPlanets);
                    PlanetMap.FajlbaIr(Settings.FilePaths.PlanetMapTXT);
                }
            }
            static void ScoutMapGeneralas()
            {
                ScoutMap.Initialize(FelderitokDetektalasa(), Settings.MapSizeX, Settings.MapSizeY);
                ScoutMap.FajlbaIr(Settings.FilePaths.ScoutMapTXT);

                ScoutMap.ScoutsNeeded = (Settings.MapSizeX / (ShipLatotav*2-1)) + (Settings.MapSizeX % ShipLatotav == 0 ? 0 : 1);
            }
        }
        static class Felderites
        {
            static List<int> statuses = new List<int>();
            static List<int> resetters = new List<int>();
            static List<int> iterators = new List<int>();
            static int counter = 0;
            public static List<BattleCommand> Felderit()
            {
                return FelderitesPaszta();
                //return FelderitesFesu();
            }

            static List<BattleCommand> FelderitesPaszta()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();
                counter++;
                if ((ScoutMap.ScoutList.Count == 0 || counter > OwnPlanets.First().IncTime * 1000) && ScoutMap.ScoutList.Count < 4)
                {
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                    statuses.Add(0);
                    resetters.Add(0);
                    iterators.Add(1);
                    counter = 0;
                }
                if (ScoutMap.ActualNumberOfScouts != 0)
                {
                    for (int i = 0; i < ScoutMap.ScoutList.Count; i++)
                    {
                        Pasztazo(parancsok, i);  
                    }
                    
                }

                return parancsok;
            }
            static void Pasztazo(List<BattleCommand> parancsok, int hanyadik)
            {
                if (iterators[hanyadik] < Settings.MapSizeY / 2)
                {
                    if (statuses[hanyadik] < 40)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, -1 + ShipLatotav, (-1 + ShipLatotav * iterators[hanyadik])));

                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 45)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, -1 + ShipLatotav, (ShipLatotav * (iterators[hanyadik] + 1) - 1)));
                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 85)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, Settings.MapSizeX - ShipLatotav, (ShipLatotav * (iterators[hanyadik] + 1) - 1)));
                        statuses[hanyadik]++;
                    }
                    else if (statuses[hanyadik] < 90)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, Settings.MapSizeX - ShipLatotav, (ShipLatotav * (iterators[hanyadik] + 2) - 1)));
                        statuses[hanyadik]++;
                    }
                    else
                    {
                        statuses[hanyadik] = 0;
                        iterators[hanyadik] += 2;
                    }
                }
                else
                {
                    if (resetters[hanyadik] > 55)
                    {
                        iterators[hanyadik] = 0;
                        resetters[hanyadik] = 0;
                    }
                    else
                    {
                        if (resetters[hanyadik] == 0)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[hanyadik].ItemId, -1 + ShipLatotav, -1 + ShipLatotav));
                        }
                        resetters[hanyadik]++;
                    }
                }
            }



            static List<BattleCommand> FelderitesFesu()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //Scoutok létrehozása:
                if (ScoutMap.ActualNumberOfScouts < ScoutMap.ScoutsNeeded)
                {
                    //Annyit választunk le, amennyi még kell ahhoz, hogy elég Felderítőnk legyen
                    int mennyiScoutKellMeg = ScoutMap.ScoutsNeeded - ScoutMap.ActualNumberOfScouts;
                    for (int i = 0; i < mennyiScoutKellMeg; i++)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }
                //Helyre küldés (Ha még nincsenek kiküldve és pont annyi scout van amennyit ki akarunk küldeni)
                if (!ScoutMap.ScoutsOut && ScoutMap.ActualNumberOfScouts == ScoutMap.ScoutsNeeded)
                {
                    for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                    {
                        parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                    }
                    ScoutMap.ScoutsOut = true;
                }
                //Mozgatás:
                if (ScoutMap.ScoutsOut && ScoutMap.AllScoutsStanding)
                {
                    if (ScoutMap.ScoutsOnRight) //Ha jobb oldalon vannak a scoutok
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, 0, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                    else
                    {
                        for (int i = 0; i < ScoutMap.ActualNumberOfScouts; i++)
                        {
                            parancsok.Add(Commands.Move(ScoutMap.ScoutList[i].ItemId, Settings.MapSizeX - 1, i * ShipLatotav * 2 + ShipLatotav / 2));
                        }
                        ScoutMap.ScoutsOnRight = !ScoutMap.ScoutsOnRight;
                    }
                }

                return parancsok;
            }
            static List<BattleCommand> FelderitesRandom()
            {
                const bool FOLYAMATOS_UTANPOTLAS = true;
                const int KEZDETI_FELDERITOK_SZAMA = 5;
                const int MAX_FELDERITOK_SZAMA = 5;
                const int KULDESI_IDOKOZ = 20; //minél nagyobb, annál lassabb, de legalább 2

                List<BattleCommand> parancsok = new List<BattleCommand>();
                List<OwnShip> felderitok = FelderitokDetektalasa();

                if (felderitok.Count() < KEZDETI_FELDERITOK_SZAMA - 1)
                {   //ha még nincs felderítőnk, akkor a bolygónkról válasszunk le egyet
                    OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                    parancsok.Add(Commands.Split(planet.ItemId, 1));
                }
                else if (Orajel % KULDESI_IDOKOZ == 0)
                {
                    foreach (OwnShip felderito in felderitok)
                    {
                        parancsok.Add(Commands.Move(felderito.ItemId, random.Next(0, 40), random.Next(0, 40)));
                    }
                }

                if (FOLYAMATOS_UTANPOTLAS && felderitok.Count < MAX_FELDERITOK_SZAMA)
                {
                    if (Orajel % (KULDESI_IDOKOZ - 1) == 0)
                    {
                        OwnPlanet planet = OwnPlanets.Where(x => x.NumberOfUnits == OwnPlanets.Max(y => y.NumberOfUnits)).First();
                        parancsok.Add(Commands.Split(planet.ItemId, 1));
                    }
                }

                return parancsok;
            }
        }
        static class Tamadas
        {
            static List<PointF> IdeTamadtunkMar = new List<PointF>();

            public static List<BattleCommand> Tamad()
            {
                List<BattleCommand> parancsok = new List<BattleCommand>();

                //FarShot takarítás és aktiválás
                FarShot.Takaritas(farShots);

                if (PlanetMap.NeutralPlanetsList.Count >= 1)
                {
                    OwnPlanet forras = OwnPlanets.Legnagyobb();
                    NeutralPlanet cel = PlanetMap.NeutralPlanetsList.First();
                    if (!IdeTamadtunkMar.Contains(new PointF(cel.PosX, cel.PosY))   //ha még nem támadtunk ide
                        && (forras.NumberOfUnits > cel.NumberOfUnits + 1))          //és tudunk
                    {
                        FarShot fs = new FarShot(forras, cel, cel.NumberOfUnits + 1);
                        farShots.Add(fs);
                        IdeTamadtunkMar.Add(new PointF(cel.PosX, cel.PosY));
                    }
                }


                for (int i = 0; i < farShots.Count; i++)
                    parancsok.Add(farShots[i].Activate(OwnShips));

                return parancsok;
            }
        }

        public static int Orajel { get; set; }
        static int ShipLatotav = 2;
        static Random random = new Random();

        static List<FarShot> farShots = new List<FarShot>();

        #region Actual Item Lists

        static List<OwnPlanet> OwnPlanets = new List<OwnPlanet>();
        static List<OwnShip> OwnShips = new List<OwnShip>();

        static List<EnemyPlanet> EnemyPlanets = new List<EnemyPlanet>();
        static List<EnemyShip> EnemyShips = new List<EnemyShip>();

        static List<NeutralPlanet> NeutralPlanets = new List<NeutralPlanet>();

        #endregion

        public static List<BattleCommand> KiadottParancsok()
        {
            List<BattleCommand> parancsok = new List<BattleCommand>();

            parancsok = parancsok.Concat(Felderites.Felderit()).ToList();
            //parancsok = parancsok.Concat(Tamadas.Tamad()).ToList();

            return parancsok;
        }

        /// <summary>
        /// Megadja a kiküldött felderítőinket. Felderítő = 1 energiaszintű saját hajó
        /// </summary>
        /// <returns></returns>
        static List<OwnShip> FelderitokDetektalasa()
        {
            List<OwnShip> lista = new List<OwnShip>();

            lista = (from x in OwnShips
                     where x.NumberOfUnits == 1      //vagy hogyan máshogy detektáljuk a felderítőinket?
                     select x).ToList();

            return lista;
        }
    }
}