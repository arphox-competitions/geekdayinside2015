﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceBattle.Common;
using System.Drawing;

namespace Teszteles02
{
    static class Traffipax
    {
        const int TRAFFI_EGYSEGEK_SZAMA = 1;

        static int counter = 0;
        static PointF traffiElozo = new PointF();
        static PointF traffiMost = new PointF();

        public static List<BattleCommand> Indit()
        {
            List<BattleCommand> parancsok = new List<BattleCommand>();
            OwnPlanet opfirst = AI2.OwnPlanets.First();
            if (counter <= 4)
            {
                if (counter == 0)
                {
                    parancsok.Add(Commands.Split(opfirst.ItemId, TRAFFI_EGYSEGEK_SZAMA)); counter++;
                }
                else if (counter == 1)
                {
                    OwnShip osfirst = AI2.OwnShips.First();

                    if (AI2.OwnShips.Count != 0)
                    {
                        if (opfirst.PosX + 5 < Settings.MapSizeX)
                        {
                            parancsok.Add(Commands.Move(osfirst.ItemId, opfirst.PosX + 5, opfirst.PosY));
                        }
                        else if (opfirst.PosY + 5 < Settings.MapSizeY)
                        {
                            parancsok.Add(Commands.Move(osfirst.ItemId, opfirst.PosX, opfirst.PosY + 5));
                        }
                        else
                        {
                            parancsok.Add(Commands.Move(osfirst.ItemId, opfirst.PosX - 5, opfirst.PosY));
                        }
                        counter++;
                    }
                }
                else if (counter == 2)
                {
                    OwnShip osfirst = AI2.OwnShips.First();

                    traffiElozo = new PointF(osfirst.PosX, osfirst.PosY);
                    counter++;
                }
                else if (counter == 3)
                {
                    OwnShip osfirst = AI2.OwnShips.First();

                    traffiMost = new PointF(osfirst.PosX, osfirst.PosY);
                    counter++;
                }
                else if (counter == 4)
                {
                    Settings.SPEED = (float)Matek.KetPontTavolsaga(traffiElozo.X, traffiElozo.Y, traffiMost.X, traffiMost.Y);
                    counter++;
                }
            }

            return parancsok;
        }
        public static bool Vege
        {
            get
            {
                return counter > 4;
            }
        }
    }
}