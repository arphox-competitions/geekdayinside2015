﻿using SpaceBattle.Common;
using System.Collections.Generic;
using System.Linq;

namespace Teszteles01
{
    class TransferShip
    {
        public OwnShip Ship { get; private set; }
        public GameItem TargetPlanet { get; private set; }

        public TransferShipState State { get; set; }

        public TransferShip(OwnShip Ship, GameItem TargetPlanet)
        {
            this.Ship = Ship;
            this.TargetPlanet = TargetPlanet;
        }

        public BattleCommand Activate(List<OwnShip> OwnShips)
        {
            //Ha már helyben vagyunk, valószínűleg sikerülni fog a szinte azonnal leadott lövés
            if (State == TransferShipState.Moved)
            {
                //Próbáljuk meg kitalálni, hogy melyik hajó jött létre:
                IEnumerable<OwnShip> matchingShips = from x in OwnShips
                                                     //biztos, hogy a kiküldött planet koordinátáján van először
                                                     where 
                                                     x.ItemId == Ship.ItemId
                                                     select x;
                if (matchingShips.Count() == 0 || matchingShips.Count() > 1)
                {
                    State = TransferShipState.Error;
                    return new CmdNop();
                }
                else  //De csak ha egyet, akkor egyértelmű, hogy melyik hajót fogjuk küldözgetni
                {
                    Ship = matchingShips.ElementAt(0);
                }

                //Ha áll a hajónk, akkor támadhatunk vele
                if (Ship.DestinationX < 0 && Ship.DestinationY < 0)
                {
                    State++;
                    return Commands.Shoot(Ship.ItemId, Ship.NumberOfUnits, TargetPlanet.ItemId);
                }

                return new CmdNop();
            }
            if (State == TransferShipState.Initial)
            {
                State++;
                return Commands.Move(Ship.ItemId, TargetPlanet.PosX, TargetPlanet.PosY);
            }
            return new CmdNop();
        }

        //STATIC:
        public static void Takaritas(List<TransferShip> transferShips)
        {
            for (int i = transferShips.Count - 1; i >= 0; i--)
            {
                if (transferShips[i].State == TransferShipState.Done || transferShips[i].State == TransferShipState.Error)
                {
                    transferShips.RemoveAt(i);
                }
            }
        }
    }

    enum TransferShipState
    {
        Initial, Moved, Done, Error
    }
}