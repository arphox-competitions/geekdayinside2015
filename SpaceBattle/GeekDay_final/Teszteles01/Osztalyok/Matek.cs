﻿using System;

namespace Teszteles01
{
    static class Matek
    {
        public static double KetPontTavolsaga(float p1x, float p1y, float p2x, float p2y)
        {
            return Math.Sqrt(Math.Pow((p1x - p2x), 2) + Math.Pow((p1y - p2y), 2));
        }
        public static double KetItemTavolsaga(GameItem g1, GameItem g2)
        {
            return KetPontTavolsaga(g1.PosX, g1.PosY, g2.PosX, g2.PosY);
        }

        public static int MennyivelKellTamadni(EnemyPlanet cel, OwnPlanet forras)
        {
            double tick = Settings.INTERVAL + 1;                        //ms, +1 a biztonság kedvéért
            double tavolsag = Matek.KetItemTavolsaga(cel, forras);      //egység
            double sebesseg = (1000 / tick) * Settings.SPEED;           //egység / sec
            double ido = tavolsag / sebesseg;                           //sec

            int darab = cel.NumberOfUnits
                        + (int)Math.Round((ido * (1 / cel.IncTime)), 0)
                        + 2; //a kerekítés miatt + 1, a biztonság kedvéért + 1

            return darab;
        }
    }
}