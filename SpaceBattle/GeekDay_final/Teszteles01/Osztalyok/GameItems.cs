﻿using System;

namespace Teszteles01
{
    class GameItem
    {
        public int ItemId { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public int NumberOfUnits { get; set; }

        public override string ToString()
        {
            return String.Format("ID={0}, Pos=({1},{2}), Units={3}", ItemId, PosX, PosY, NumberOfUnits);
        }

        public override bool Equals(object obj)
        {
            return (((GameItem)obj).ItemId == this.ItemId);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    abstract class OwnGameItem : GameItem
    {
        public override string ToString()
        {
            return base.ToString();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    class OwnPlanet : OwnGameItem
    {
        public float IncTime { get; set; }      // How many seconds are required to increase the power level - only for planets.
        public override string ToString()
        {
            return base.ToString() + ", IncTime=" + IncTime;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    class OwnShip : OwnGameItem
    {
        public float DestinationX { get; set; } //Cél X koordinátája, csak hajóknál. -1 megállást jelent.
        public float DestinationY { get; set; } //Cél Y koordinátája, csak hajóknál. -1 megállást jelent.
        public override string ToString()
        {
            return base.ToString() + String.Format(", Dest=({0},{1})");
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    abstract class EnemyGameItem : GameItem
    {
        public string PlayerName { get; set; }  //Tulajdonos játékos neve. String.Empty ha senki

        public override string ToString()
        {
            return base.ToString() + ", Player=" + PlayerName;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    class EnemyPlanet : EnemyGameItem
    {
        public float IncTime { get; set; }      // How many seconds are required to increase the power level - only for planets.
        public override string ToString()
        {
            return base.ToString() + ", IncTime=" + IncTime;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    class EnemyShip : EnemyGameItem
    {
        public float DestinationX { get; set; } //Cél X koordinátája, csak hajóknál. -1 megállást jelent.
        public float DestinationY { get; set; } //Cél Y koordinátája, csak hajóknál. -1 megállást jelent.
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString() + String.Format(", Dest=({0},{1})");
        }
    }

    class NeutralPlanet : GameItem
    {
        public float IncTime { get; set; }      // How many seconds are required to increase the power level - only for planets.

        public override string ToString()
        {
            return base.ToString() + ", IncTime=" + IncTime;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}